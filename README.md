 # Simple todo release notes

 ## Play Store
 [check on Play Store](https://play.google.com/store/apps/details?id=com.pericat.widget.todowidget)

## 2.0.0
 - Complete design review
 - Add numbers/mails/links actions
 - Clipboard action
 - Alert date small bug fix when editing a task
 
 ## 1.1.0
 - Edition mode
 - New "more actions" UI
 - Bug fixes for older android versions

# 1.0.0
- First release
- Basic functionalities
