package com.pericat.widget.simpletodo.helpers

import android.content.Context
import android.text.format.DateFormat
import java.util.*

class HelperDate {

    companion object {

        /**
         * Permet de retourner la chaîne de caractère correspondant
         * à la date et à la culture actuelle
         */
        fun getStringDateCurrentCulture(context: Context, date: Date): String {

            // Paramétrage du
            var dateFormat = DateFormat.getDateFormat(context)
            var timeFormat = DateFormat.getTimeFormat(context)

            return dateFormat.format(date) + " " + timeFormat.format(date)
        }

        /**
         * Permet de récupérer un objet date à partir des paramètres spécifiés
         */
        fun createDate(year: Int, month: Int, day: Int, hours: Int, minutes: Int): Date {

            // Créer et retourne une Date
            val calendar = Calendar.getInstance()
            calendar.set(year, month, day, hours, minutes)

            return calendar.time
        }

    }
}