package com.pericat.widget.simpletodo

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.pericat.widget.simpletodo.helpers.HelperDate
import com.pericat.widget.simpletodo.helpers.HelperLayout
import com.pericat.widget.simpletodo.helpers.HelperSharedPreferences
import com.pericat.widget.simpletodo.helpers.StaticVariables
import com.pericat.widget.simpletodo.model.TodoTask
import java.util.*
import java.util.logging.Logger


class NewTaskActivity : AppCompatActivity() {

    var log: Logger = Logger.getLogger(StaticVariables.LOGGER_NAME)

    // Déclaration des vues
    internal lateinit var et_content: EditText
    internal lateinit var et_title: EditText
    internal lateinit var ll_parent: LinearLayout
    internal lateinit var bt_ok: Button
    internal lateinit var ibt_alert: ImageButton
    internal lateinit var ll_alert: LinearLayout
    internal lateinit var tv_alert: TextView

    // Déclaration des valeurs demandées à l'utilisateur
    internal var alert_date: Date? = null
    internal var alert_year: Int = 0
    internal var alert_month: Int = 0
    internal var alert_day: Int = 0


    // variable global
    internal var editingTask: TodoTask? = null
    internal var editingTaskPosition: Int? = null

    internal var api19_bugfix: Boolean = false

    // region constructor

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Window.FEATURE_NO_TITLE
        setContentView(R.layout.activity_new_task)

        // Récupère l'editTexte du contenu
        et_content = findViewById(R.id.NewTaskAct_ET_content)
        et_title = findViewById(R.id.NewTaskAct_ET_title)
        ll_parent = findViewById(R.id.NewTaskAct_LL_parent)
        bt_ok = findViewById(R.id.NewTaskAct_BT_Ok)
        ibt_alert = findViewById(R.id.NewTaskAct_IBT_alert)
        ll_alert = findViewById(R.id.NewTaskAct_LL_alert)
        tv_alert = findViewById(R.id.NewTaskAct_TV_alert)

        // Ajoute l'évènement au click de ok
        bt_ok.setOnClickListener(ok_ClickListener)

        // Ajout l'évènement de création de l'alert (ouverture de la popup)
        ibt_alert.setOnClickListener(alert_ClickListener)
        ll_alert.setOnClickListener(alert_ClickListener)

        // Demande le focus pour afficher le clavier
        et_title.requestFocus()

        // essaie de récupérer la position (si mode EDITION)
        val bundle = intent.extras

        editingTaskPosition = bundle?.getInt(StaticVariables.EXTRA_TASK_EDITING_POSITION)

        // MODE EDITION
        if (editingTaskPosition != null) {

            // récupère la task
            val currentTodoList: ArrayList<TodoTask> = ArrayList(HelperSharedPreferences.getTodoList(this, StaticVariables.SHARED_TODOLIST))
            editingTask = currentTodoList.get(editingTaskPosition!!)

            if (editingTask != null) {
                et_title.setText(editingTask!!._title)
                et_content.setText(editingTask!!._content)

                if (editingTask!!._alert != null) {
                    alert_date = editingTask?._alert
                    tv_alert.text = HelperDate.getStringDateCurrentCulture(applicationContext, editingTask!!._alert!!)
                }
            }
        }

        // Ajoute les listeners de saisie
        et_title.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(string: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (string?.length!! >= 1) {
                    bt_ok.isEnabled = true
                } else if (et_content.length() < 1) {
                    bt_ok.isEnabled = false
                }
            }
        })

        et_content.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(string: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (string?.length!! >= 1) {
                    bt_ok.isEnabled = true
                } else if (et_title.length() < 1) {
                    bt_ok.isEnabled = false
                }
            }
        })
    }

    // endregion

    // region Listeners

    /**
     * TouchListener permettant l'ajout de la tâche
     */
    val ok_ClickListener: View.OnClickListener = View.OnClickListener { it ->

        try {

            // Création de l'intent personnalisé pour renvoyer le résultat
            val i = Intent(this, TodoWidgetProvider::class.java)
            i.action = StaticVariables.ACTION_NEWTASK

            // Ajoute les données de la nouvelle tâche en extra
            i.putExtra(StaticVariables.EXTRA_NEWTODO_TITLE, et_title.text?.toString()!!)
            i.putExtra(StaticVariables.EXTRA_NEWTODO_CONTENT, et_content.text?.toString()!!)
            i.putExtra(StaticVariables.EXTRA_NEWTODO_ALERT, alert_date?.time)

            // Si nous étions en mode EDITION
            if (isEdition()) {
                i.putExtra(StaticVariables.EXTRA_TASK_EDITING_POSITION, editingTaskPosition!!)
            }

            // Envoie l'intent pour que tous les receivers potentiels les reçoivent
            sendBroadcast(i)

            // Ferme l'activité
            finish()

        } catch (e: Exception) {
            log.severe("Error when ok click.")
        }
    }

    /**
     * Cette méthode permet d'ouvrir les popups de sélection de date et heure
     */
    val alert_ClickListener: View.OnClickListener = View.OnClickListener { it ->

        bt_ok.isEnabled = true
        // Lance la sélection de la date
        showDatePicker()
    }

    // endregion

    // region Helpers


    /**
     * Permet de lancer la popup de sélection de la date (année mois jour)
     */
    private fun showDatePicker() {

        HelperLayout().hideSoftKeyboard(this)
        // Récupère la date du jour
        val c = Calendar.getInstance()

        // si mode édition récupère la bonne date
        if (isEdition() && editingTask?._alert != null) {
            c.time = editingTask?._alert
        }

        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        // Déclare la boite de dialog de sélection à partir de la date du jour
        val dialog = DatePickerDialog(this, R.style.MyDatePickerDialogTheme,

                // Action a éffectuer une fois la date sélectionnée
                DatePickerDialog.OnDateSetListener { view, yearSelected, monthOfYear, dayOfMonth ->

                    // Affecte les
                    alert_year = yearSelected
                    alert_month = monthOfYear
                    alert_day = dayOfMonth

                    if (!api19_bugfix) {
                        // Lance le picker de time
                        showTimePicker()
                    }

                }, year, month, day)

        // Ouvre la dialog
        dialog.show()
    }

    private fun isEdition() = editingTaskPosition != null && editingTask != null

    /**
     * Permet de lancer la popup de sélection de la date (année mois jour)
     */
    private fun showTimePicker() {

        api19_bugfix = true
        // Récupère la date du jour
        val c = Calendar.getInstance()

        // si mode édition récupère la bonne date
        if (isEdition() && editingTask?._alert != null) {
            c.time = editingTask!!._alert
        }

        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        // Déclare la boite de dialog de sélection à partir de la date du jour
        val dialog = TimePickerDialog(this, R.style.MyTimePickerDialogTheme,

                // Action a éffectuer une fois la date sélectionnée
                TimePickerDialog.OnTimeSetListener { timePicker, hourSet, minuteSet ->

                    // Créer une nouvelle date
                    alert_date = HelperDate.createDate(alert_year, alert_month, alert_day, hourSet, minuteSet)

                    if (alert_date != null) {

                        // Récupère une chaîne pour l'affichage
                        val stringDate = HelperDate.getStringDateCurrentCulture(this, alert_date!!)
                        tv_alert.text = stringDate
                    }

                    api19_bugfix = false

                }, hour, minute, true)

        // Ouvre la dialog
        dialog.show()
    }

    // endregion

    // region overrides

    // endregion

}
