package com.pericat.widget.simpletodo.helpers

/**
 * Classe représentant les variables statiques de l'applications
 */
class StaticVariables {
    companion object {

        val LOGGER_NAME = "Simple Todo"


        // #-------------#
        // # Les intents #
        // #-------------#
        // Global au widget
        val ACTION_NEWTASK = "com.pericat.todowidget.NEWTASK"
        val ACTION_STARTACTIVITY_NEWTASK = "com.pericat.todowidget.STARTACTIVITY_NEWTASK"
        val ACTION_ALERT_TASK = "com.pericat.todowidget.ALERT_TASK"

        // Utilisé dans les "items"
        val ACTION_ITEMCLIKED = "com.pericat.todowidget.ITEMCLICKED"
        val EXTRA_BUTTON_TYPE_KEY = "BUTTON_TYPE_CLICKED"
        val EXTRA_BUTTON_MORE = "BUTTON_MORE"
        val EXTRA_BUTTON_EDIT = "BUTTON_EDIT"
        val EXTRA_BUTTON_CLIPBOARD = "BUTTON_CLIPBOARD"
        val EXTRA_BUTTON_DELETE = "BUTTON_DELETE"
        val EXTRA_BUTTON_NOBUTTON = "BUTTON_NOBUTTON"
        val EXTRA_TASK_EDITING_POSITION = "TASK_EDITING"

        // Paramètres des intents
        val EXTRA_NEWTODO_TITLE: String = "NEWTODO_TITLE"
        val EXTRA_NEWTODO_CONTENT: String = "NEWTODO_CONTENT"
        val EXTRA_NEWTODO_ALERT: String = "NEWTODO_ALERT"
        val EXTRA_TASK_POSITION: String = "EXTRA_TASK_POSITION"
        val EXTRA_ALERT_TASK = "EXTRA_ALERT_TASK"

        // #-----------------------#
        // # Les sharedPreferences #
        // #-----------------------#
        val SHARED_NAME = "com.pericat.todowidget.SHAREDPREFERENCES"
        val SHARED_TODOLIST = "todolist"
        val SHARED_ALARMLIST = "alarmList"


        // #-------------------#
        // # Les notifications #
        // #-------------------#
        val NOTIFICATION_CHANNELNAME = "TODOWIDGET_NOTIFICATION"
        val NOTIFICATION_CHANNELID = "125125"
    }
}