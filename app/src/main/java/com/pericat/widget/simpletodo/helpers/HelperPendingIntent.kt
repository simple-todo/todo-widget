package com.pericat.widget.simpletodo.helpers

import android.app.PendingIntent
import android.content.Context
import android.content.Intent

class HelperPendingIntent {

    companion object {

        /**
         * Permet de créer un pendingIntent
         */
        fun getPendingSelfIntent(context: Context, classe: Class<Any>, action: String): PendingIntent {
            val intent = Intent(context, classe)
            intent.action = action
            return PendingIntent.getBroadcast(context, 0, intent, 0)
        }
    }

}