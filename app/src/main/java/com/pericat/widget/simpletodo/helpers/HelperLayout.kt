package com.pericat.widget.simpletodo.helpers

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AutoCompleteTextView
import android.widget.EditText

class HelperLayout {

    /**
     * Permet de cacher le softKeyboard
     * @param activity
     */
    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager = activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken, 0)
    }

    /**
     * Mets en place des listener de tous les ViewGroupe fils de "view"
     * @param view
     */
    fun closeActivityOnTouch(view: View, activity: Activity) {

        // Ajoute les listeners
        if (view !is EditText || view is AutoCompleteTextView) {
            view.setOnTouchListener { _, _ ->
                hideSoftKeyboard(activity)
                false
            }
        }

        // Si c'est un container on lui donne l listener
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                closeActivityOnTouch(innerView, activity)
            }
        }
    }
}