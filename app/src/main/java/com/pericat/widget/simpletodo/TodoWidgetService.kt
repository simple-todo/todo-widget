package com.pericat.widget.simpletodo

import android.content.Intent
import android.widget.RemoteViewsService
import com.pericat.widget.simpletodo.adapter.TodoListWidgetProvider


/**
 * Created by Etienne on 04/03/2018.
 */
class TodoWidgetService : RemoteViewsService() {

    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return TodoListWidgetProvider(this)
    }


}