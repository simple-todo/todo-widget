package com.pericat.widget.simpletodo.adapter

import android.content.Context
import android.content.Intent
import android.text.format.DateUtils
import android.view.View
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.core.content.ContextCompat
import com.pericat.widget.simpletodo.R
import com.pericat.widget.simpletodo.helpers.HelperSharedPreferences
import com.pericat.widget.simpletodo.helpers.StaticVariables
import com.pericat.widget.simpletodo.model.TodoTask
import java.util.logging.Logger


/**
 * Cette classe représente en quelques sorte l'adapter que l'on connait bien pour les listview dans une application
 * android lambda. Elle permet donc de remplir la liste avec les données
 */
class TodoListWidgetProvider(context: Context) : RemoteViewsService.RemoteViewsFactory {

    var log: Logger = Logger.getLogger(StaticVariables.LOGGER_NAME)
    var _items: ArrayList<TodoTask> = ArrayList()
    var _context: Context? = context

    override fun onCreate() {

        // Récupère la liste
        _items = ArrayList(HelperSharedPreferences.getTodoList(_context!!, StaticVariables.SHARED_TODOLIST))
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewAt(position: Int): RemoteViews {

        // Récupère l'item correspondant dans la liste
        val task = _items.get(position)

        val itemView: RemoteViews?

        // Vue "more actions"
        if (task._editing) {
            itemView = setUpMoreActionsView(position)
            itemView.setTextColor(R.id.Adapter_TV_title, _context?.let { ContextCompat.getColor(it, R.color.App_GrayLight) }!!)
            itemView.setTextColor(R.id.Adapter_TV_content, _context?.let { ContextCompat.getColor(it, R.color.App_GrayLight) }!!)
            itemView.setTextColor(R.id.Adapter_TV_alertDate, _context?.let { ContextCompat.getColor(it, R.color.App_GrayLight) }!!)
        }

        // Vue lecture
        else {
            itemView = setUpClassicalView(task, position)
        }

        // Ajuste la visibilité des éléments
        adjustViewsVisibility(itemView, task)

        // Ajoute le contenu
        setViewsContent(itemView, task, position)

        return itemView
    }

    override fun getCount(): Int {
        return _items.size
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }

    override fun onDataSetChanged() {

        // Récupère la liste mise à jour
        _items = ArrayList(HelperSharedPreferences.getTodoList(_context!!, StaticVariables.SHARED_TODOLIST))
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getViewTypeCount(): Int {
        return 2
    }

    override fun onDestroy() {
    }


    /**
     * Ajuste la visibilité de certains composants des items
     * en fonction du contenu de la tâche
     */
    private fun adjustViewsVisibility(itemView: RemoteViews?, task: TodoTask) {

        // alert visibility
        val alertVisibility = if (task._alert == null) View.GONE else View.VISIBLE
        itemView?.setViewVisibility(R.id.Adapter_I_alert, alertVisibility)
        itemView?.setViewVisibility(R.id.Adapter_TV_alertDate, alertVisibility)

        // content visibility
        val contentVisibility = if (task._content.isNullOrBlank()) View.GONE else View.VISIBLE
        itemView?.setViewVisibility(R.id.Adapter_TV_content, contentVisibility)

        // title visibility
        val titleVisibility = if (task._title.isNullOrBlank()) View.GONE else View.VISIBLE
        itemView?.setViewVisibility(R.id.Adapter_TV_title, titleVisibility)

    }


    private fun setViewsContent(itemView: RemoteViews, task: TodoTask, position: Int) {

        // Ajoute le contenu
        itemView.setTextViewText(R.id.Adapter_TV_title, task._title)
        itemView.setTextViewText(R.id.Adapter_TV_content, task._content)

        var datetime = ""
        if (task._alert != null) {
            datetime = DateUtils.formatDateTime(_context, task._alert?.time!!,
                    DateUtils.FORMAT_SHOW_DATE or
                            DateUtils.FORMAT_NUMERIC_DATE or
                            DateUtils.FORMAT_SHOW_TIME)

        }
        itemView.setTextViewText(R.id.Adapter_TV_alertDate, datetime)

        // Ajoute l'intent commun au 2 items
        val noActionIntent = Intent()
        noActionIntent.putExtra(StaticVariables.EXTRA_TASK_POSITION, position)
        noActionIntent.putExtra(StaticVariables.EXTRA_BUTTON_TYPE_KEY, StaticVariables.EXTRA_BUTTON_NOBUTTON)
        itemView.setOnClickFillInIntent(R.id.Adapter_RL_parent, noActionIntent)
    }

    /**
     * Prépare la vue de l'item en mode "more actions".
     * Les paramètres de ces Intents sont défini par ItemPendingTemplate dans @see TodoWidgetProvider
     */
    private fun setUpMoreActionsView(position: Int): RemoteViews {
        // Récupère une itemView de la ligne
        var itemView = RemoteViews(
                _context?.packageName, R.layout.todotask_adapter_editing)

        val deleteClickIntent = Intent()
        deleteClickIntent.putExtra(StaticVariables.EXTRA_TASK_POSITION, position)
        deleteClickIntent.putExtra(StaticVariables.EXTRA_BUTTON_TYPE_KEY, StaticVariables.EXTRA_BUTTON_DELETE)
        itemView.setOnClickFillInIntent(R.id.Adapter_IBT_delete, deleteClickIntent)

        val editClickIntent = Intent()
        editClickIntent.putExtra(StaticVariables.EXTRA_TASK_POSITION, position)
        editClickIntent.putExtra(StaticVariables.EXTRA_BUTTON_TYPE_KEY, StaticVariables.EXTRA_BUTTON_EDIT)
        itemView.setOnClickFillInIntent(R.id.Adapter_IBT_edit, editClickIntent)

        val clipboardClickIntent = Intent()
        clipboardClickIntent.putExtra(StaticVariables.EXTRA_TASK_POSITION, position)
        clipboardClickIntent.putExtra(StaticVariables.EXTRA_BUTTON_TYPE_KEY, StaticVariables.EXTRA_BUTTON_CLIPBOARD)
        itemView.setOnClickFillInIntent(R.id.Adapter_IBT_copy, clipboardClickIntent)

        return itemView
    }

    /**
     * Prépare la vue de l'item de base
     */
    private fun setUpClassicalView(task: TodoTask, position: Int): RemoteViews {
        // Récupère une remoteView de la ligne
        var itemView = RemoteViews(
                _context?.packageName, R.layout.todotask_adapter)

        val moreClickIntent = Intent()
        moreClickIntent.putExtra(StaticVariables.EXTRA_TASK_POSITION, position)
        moreClickIntent.putExtra(StaticVariables.EXTRA_BUTTON_TYPE_KEY, StaticVariables.EXTRA_BUTTON_MORE)
        itemView.setOnClickFillInIntent(R.id.Adapter_IBT_more, moreClickIntent)
        return itemView
    }
}