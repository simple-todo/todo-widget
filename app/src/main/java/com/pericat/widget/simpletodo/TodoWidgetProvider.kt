package com.pericat.widget.simpletodo

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.*
import android.net.Uri
import android.os.Build
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.gson.Gson
import com.pericat.widget.simpletodo.helpers.HelperAlarm
import com.pericat.widget.simpletodo.helpers.HelperSharedPreferences
import com.pericat.widget.simpletodo.helpers.StaticVariables
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.ACTION_ALERT_TASK
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.ACTION_ITEMCLIKED
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.ACTION_NEWTASK
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.ACTION_STARTACTIVITY_NEWTASK
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.EXTRA_BUTTON_CLIPBOARD
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.EXTRA_BUTTON_DELETE
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.EXTRA_BUTTON_EDIT
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.EXTRA_BUTTON_MORE
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.EXTRA_BUTTON_NOBUTTON
import com.pericat.widget.simpletodo.helpers.StaticVariables.Companion.EXTRA_BUTTON_TYPE_KEY
import com.pericat.widget.simpletodo.model.TodoTask
import java.util.*
import java.util.logging.Logger
import kotlin.collections.ArrayList


/**
 * Created by Etienne on 28/02/2018.
 */
class TodoWidgetProvider : AppWidgetProvider() {

    var log: Logger = Logger.getLogger(StaticVariables.LOGGER_NAME)

    override fun onEnabled(context: Context?) {
        super.onEnabled(context)

        log.info("Enabling widget")

        try {

            val filter = IntentFilter()
            context?.applicationContext?.registerReceiver(this, filter)

            // Récupère les informations à partir du manager
            val manager = AppWidgetManager.getInstance(context)
            val ids = manager.getAppWidgetIds(ComponentName(context?.applicationContext?.packageName.toString(), javaClass.name))

            // Lance la mise à jour
            onUpdate(context, manager, ids)
        } catch (e: Exception) {
            log.severe("Error during widget initialization")
        }
    }


    override fun onUpdate(context: Context?, appWidgetManager: AppWidgetManager?, appWidgetIds: IntArray?) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        log.info("Updating widget")

        try {
            // Sauvegarde la liste en mode "non édition"
            if (context != null) {
                var tasks: ArrayList<TodoTask> = HelperSharedPreferences.getTodoList(context, StaticVariables.SHARED_TODOLIST)

                tasks.forEach { it._editing = false }

                HelperSharedPreferences.saveTodoList(context, tasks, StaticVariables.SHARED_TODOLIST)
            }

            appWidgetIds?.forEach { id ->

                // Met en place l'intent qui lance le service. Le service
                // Se chargera de fournir les données pour remplir la liste
                val intent = Intent(context, TodoWidgetService::class.java)

                // Fournit l'id du widget
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id)
                intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))

                // Récupère le layout
                val layout = RemoteViews(context?.packageName, R.layout.todowidget)

                // Applique l'adapter (via l'intent) sur la listview
                layout.setRemoteAdapter(R.id.Main_LV_Todotasks, intent)

                // Template permettant de gérer les listener sur les ITEMS
                // L'intent est créer vers TodoWidgetProvider puisque le Provider est AUSSI un Broadcast receiver
                // Event suppression
                val deleteIntentTemplate = Intent(context, TodoWidgetProvider::class.java)
                deleteIntentTemplate.action = StaticVariables.ACTION_ITEMCLIKED
                layout.setPendingIntentTemplate(R.id.Main_LV_Todotasks,
                        PendingIntent.getBroadcast(context, 11, deleteIntentTemplate, PendingIntent.FLAG_UPDATE_CURRENT))

                // Event edition
                val editIntentTemplate = Intent(context, TodoWidgetProvider::class.java)
                editIntentTemplate.action = StaticVariables.ACTION_ITEMCLIKED
                layout.setPendingIntentTemplate(R.id.Main_LV_Todotasks,
                        PendingIntent.getBroadcast(context, 2, editIntentTemplate, PendingIntent.FLAG_UPDATE_CURRENT))

                // Event clipboard
                val clipboardIntentTemplate = Intent(context, TodoWidgetProvider::class.java)
                clipboardIntentTemplate.action = StaticVariables.ACTION_ITEMCLIKED
                layout.setPendingIntentTemplate(R.id.Main_LV_Todotasks,
                        PendingIntent.getBroadcast(context, 3, clipboardIntentTemplate, PendingIntent.FLAG_UPDATE_CURRENT))

                // Event more
                val moreIntentTemplate = Intent(context, TodoWidgetProvider::class.java)
                moreIntentTemplate.action = StaticVariables.ACTION_ITEMCLIKED
                layout.setPendingIntentTemplate(R.id.Main_LV_Todotasks,
                        PendingIntent.getBroadcast(context, 1997, moreIntentTemplate, PendingIntent.FLAG_UPDATE_CURRENT))

                // Item clicked without specific action
                val noActionIntentTemplate = Intent(context, TodoWidgetProvider::class.java)
                noActionIntentTemplate.action = StaticVariables.ACTION_ITEMCLIKED
                layout.setPendingIntentTemplate(R.id.Main_LV_Todotasks,
                        PendingIntent.getBroadcast(context, 12345, noActionIntentTemplate, PendingIntent.FLAG_UPDATE_CURRENT))

                // Ajoute un clickListener sur le bouton (listener du widget et non des "items")
                val intentAddNew = Intent(context, javaClass)
                intentAddNew.action = ACTION_STARTACTIVITY_NEWTASK
                layout.setOnClickPendingIntent(R.id.Main_IBT_Add,
                        PendingIntent.getBroadcast(context?.applicationContext, 10101, intentAddNew, 0))

                // Applique le layout
                appWidgetManager?.updateAppWidget(id, layout)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            log.warning("Error while updating widget.")
        }

    }

    /**
     * Méthode représentant l'implémentation de la classe BroadcastReceiver
     *
     * Permet de récupérer les intents correspondants aux actions de l'utilisateur
     * pour effectuer les traitements en conséquences
     */
    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)

        // Si le bouton d'ajout a été clické
        if (ACTION_STARTACTIVITY_NEWTASK.equals(intent?.action)) {

            // Démarrage de l'activité nouvelle task (qui simule une popup)
            val i = Intent(context, NewTaskActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)

            context?.startActivity(i)
        }

        // Si l'utilisateur à entré une nouvelle tâche
        if (ACTION_NEWTASK.equals(intent?.action)) {

            // Récupère les données
            var todoTitle = intent?.extras?.get(StaticVariables.EXTRA_NEWTODO_TITLE) as String?
            var todoContent = intent?.extras?.get(StaticVariables.EXTRA_NEWTODO_CONTENT) as String?
            var todoAlert = intent?.extras?.get(StaticVariables.EXTRA_NEWTODO_ALERT) as Long?
            var editPosition = intent?.extras?.get(StaticVariables.EXTRA_TASK_EDITING_POSITION) as Int?

            // Lance l'ajout
            addNewTask(context!!, todoTitle, todoContent, todoAlert, editPosition)
        }

        // Si l'utilisateur a cliqué sur un des items
        if (ACTION_ITEMCLIKED.equals(intent?.action)) {

            // Récupère le type de bouton cliqué
            val buttonType = intent?.getStringExtra(EXTRA_BUTTON_TYPE_KEY)
            val position = intent?.extras?.get(StaticVariables.EXTRA_TASK_POSITION) as Int?

            // Si l'utilisateur a cliqué sur "more"
            if (EXTRA_BUTTON_MORE.equals(buttonType) && position != null && context != null) {
                more(context, position)
            }

            // Si l'utilisateur veut editer la tâche
            if (EXTRA_BUTTON_EDIT.equals(buttonType) && position != null && context != null) {
                edit(context, position)
            }

            // Si l'utilisateur veut copier le contenu
            if (EXTRA_BUTTON_CLIPBOARD.equals(buttonType) && position != null && context != null) {
                clipboardCopy(context, position)
            }

            // Si l'utilisateur veut supprimer la tâche
            if (EXTRA_BUTTON_DELETE.equals(buttonType) && position != null && context != null) {
                // Lance la suppression
                deleteTask(context, position)
            }

            // Si l'utilisateur a cliqué sur l'item (mais pas sur un bouton)
            if (EXTRA_BUTTON_NOBUTTON.equals(buttonType) && position != null && context != null) {
                noAction(context, position)
            }
        }

        // Si le téléphone a été redémaré
        if ("android.intent.action.BOOT_COMPLETED".equals(intent?.action)) {

            // Récupére une liste des tâches enregistrées
            var currentTodoList: ArrayList<TodoTask> = ArrayList(HelperSharedPreferences.getTodoList(context!!, StaticVariables.SHARED_TODOLIST))
            var currentAlarmList = ArrayList<Date>()

            // Si il y avait des alarmes enregistrées
            if (currentTodoList.size > 0) {

                // Parcours des alarmes
                currentTodoList.forEachIndexed { index, task ->

                    if (task._alert != null) {

                        // Réajoute l'alarme
                        HelperAlarm.createAlarm(context, task, index)
                        currentAlarmList.add(task._alert!!)
                    }
                }
            }

            // Réenregistre la liste des alarmes
            HelperSharedPreferences.saveAlarmList(context, currentAlarmList, StaticVariables.SHARED_ALARMLIST)
        }

        // Si on reçoit un intent pour envoyer une notification
        if (ACTION_ALERT_TASK.equals(intent?.action)) {

            // Récupération de la Task sérializée
            val taskString = intent?.extras?.getString(StaticVariables.EXTRA_ALERT_TASK)

            // Désérialization
            val notificationManager = NotificationManagerCompat.from(context!!)

            val gson = Gson()
            val task: TodoTask = gson.fromJson(taskString, TodoTask::class.java)
            // Création du channel (API > 26)
            createNotificationChannel(context)

            // Création de la notification
            val mBuilder = NotificationCompat.Builder(context, StaticVariables.NOTIFICATION_CHANNELID)
                    .setSmallIcon(R.drawable.ic_alert)
                    .setContentTitle(task._title)
                    .setContentText(task._content)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(task._content))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            notificationManager.notify(5258, mBuilder.build())

        }
    }

    /**
     * Permet de créer une channel (nécessaire au fonctionnement des notifications pour les api > Oreo)
     */
    private fun createNotificationChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            var name = StaticVariables.NOTIFICATION_CHANNELNAME

            var importance = NotificationManager.IMPORTANCE_DEFAULT
            var channel = NotificationChannel(StaticVariables.NOTIFICATION_CHANNELID, name, importance)

            // Création de la channel notification
            var notificationManager = context.getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }


    /**
     * Cette méthode permet de lancer l'ajout de la nouvelle tâche
     */
    private fun addNewTask(context: Context, titre: String?, contenu: String?, alert: Long?, editPosition: Int?) {

        // par simplicité on supprime
        if (editPosition != null) {
            deleteTask(context, editPosition)
        }

        // Création de la nouvelle task
        val newTask = TodoTask(titre, contenu, alert)

        // Maintenant on ajoute l'alerte
        HelperAlarm.createAlarm(context, TodoTask(titre, contenu, alert), editPosition)

        // #--------------------------------#
        // # Gestion de la liste des tâches #
        // #--------------------------------#
        // Récupère la liste des tâches globale depuis les shared
        var currentTodoList: ArrayList<TodoTask> = ArrayList(HelperSharedPreferences.getTodoList(context, StaticVariables.SHARED_TODOLIST))

        // Si il n'y as pas de liste déjà enregistrée
        if (currentTodoList == null) currentTodoList = ArrayList()

        // Ajout la nouvelle tâche
        val addPosition = if (editPosition != null) editPosition else 0
        currentTodoList.add(addPosition, newTask)

        // Sauvegarde la liste des tâches
        HelperSharedPreferences.saveTodoList(context, currentTodoList, StaticVariables.SHARED_TODOLIST)

        // #--------------------------------#
        // # Gestion de la liste des alarmes #
        // #--------------------------------#
        if (newTask._alert != null) {
            // Récupère la liste des alarmes goable
            var currentAlarmList: ArrayList<Date?> = ArrayList(HelperSharedPreferences.getAlarmList(context, StaticVariables.SHARED_ALARMLIST))
            if (currentAlarmList == null) currentAlarmList = ArrayList()

            // Ajout de l'alarme à la liste
            currentAlarmList.add(newTask._alert)

            // Sauvegarde la liste des alarmes
            HelperSharedPreferences.saveAlarmList(context, currentAlarmList, StaticVariables.SHARED_ALARMLIST)
        }

        // Mise à jour
        updateWidgetsLists(context)
    }

    /**
     * est appelé quand "edit" button est clické
     */
    private fun edit(context: Context, position: Int) {

        // Démarrage de l'activité nouvelle task (qui simule une popup)
        // en mode EDITION
        val i = Intent(context, NewTaskActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        i.putExtra(StaticVariables.EXTRA_TASK_EDITING_POSITION, position)

        context.startActivity(i)
    }

    /**
     * est appelé quand "more" button est clické
     */
    private fun more(context: Context, position: Int) {

        // Récupère la tâche
        val currentTodoList: ArrayList<TodoTask> = ArrayList(HelperSharedPreferences.getTodoList(context, StaticVariables.SHARED_TODOLIST))
        val todo = currentTodoList.get(position)
        todo._editing = true

        // Sauvegarde la liste
        HelperSharedPreferences.saveTodoList(context, currentTodoList, StaticVariables.SHARED_TODOLIST)

        // Mise à jour
        updateWidgetsLists(context)
    }


    /**
     * Ajoute le contenu de l'item clické au clipboard
     */
    private fun clipboardCopy(context: Context, position: Int) {
        // Récupère la tâche
        val currentTodoList: ArrayList<TodoTask> = ArrayList(HelperSharedPreferences.getTodoList(context, StaticVariables.SHARED_TODOLIST))
        val todo = currentTodoList.get(position)
        todo._editing = false

        // Ajoute au clipboard
        val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText("content", todo._title + todo._content)
        clipboardManager.setPrimaryClip(clip)

        // Mise à jour
        HelperSharedPreferences.saveTodoList(context, currentTodoList, StaticVariables.SHARED_TODOLIST)
        updateWidgetsLists(context)
    }

    /**
     * Gère la réception d'un évènement "noAction" (clique hors bouton)
     */
    private fun noAction(context: Context, position: Int) {

        // Récupère la liste depuis les shared
        val currentTodoList: ArrayList<TodoTask> = ArrayList(HelperSharedPreferences.getTodoList(context, StaticVariables.SHARED_TODOLIST))

        // Supprime le mode "editing"
        currentTodoList.forEach { it._editing = false }

        // Sauvegarde la liste
        HelperSharedPreferences.saveTodoList(context, currentTodoList, StaticVariables.SHARED_TODOLIST)

        // Mise à jour
        updateWidgetsLists(context)
    }

    /**
     * Cette méthode permet de lancer la suppression de la tâche voulu
     */
    private fun deleteTask(context: Context, position: Int) {

        // Récupère la tâche
        val currentTodoList: ArrayList<TodoTask> = ArrayList(HelperSharedPreferences.getTodoList(context, StaticVariables.SHARED_TODOLIST))
        val task = currentTodoList.get(position)

        // Supprime l'alarme correspondante
        HelperAlarm.removeAlarm(context, task, position)

        // Supprime de la liste
        currentTodoList.removeAt(position)

        // Sauvegarde la liste
        HelperSharedPreferences.saveTodoList(context, currentTodoList, StaticVariables.SHARED_TODOLIST)

        // Mise à jour
        updateWidgetsLists(context)

    }


    // # -- Utils -- # //

    private fun updateWidgetsLists(context: Context) {

        // Récupèration de tous les widgets
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val appWidgetIds = appWidgetManager.getAppWidgetIds(ComponentName(context, TodoWidgetProvider::class.java))

        // Demande de mise à jour à l'adapter
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.Main_LV_Todotasks)
    }
}