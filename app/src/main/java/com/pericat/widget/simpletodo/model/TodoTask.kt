package com.pericat.widget.simpletodo.model

import java.util.*

/**
 * Created by Etienne on 03/03/2018.
 */
class TodoTask(title: String?, content: String?, alert: Long?) {

    // Private attributes
    var _title: String? = title
    var _content: String? = content
    var _editing: Boolean = false
    var _alert: Date? = if (alert != null) Date(alert) else null

    var _isDone: Boolean = false


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TodoTask

        if (_title != other._title) return false
        if (_content != other._content) return false
        if (_editing != other._editing) return false
        if (_alert != other._alert) return false
        if (_isDone != other._isDone) return false

        return true
    }

    /**
     * Return a "unique" number based on the
     * position in the list of tasks.
     *
     * Used to cancel the alarm linked to the task
     */
    fun getPositionCode(position: Int): Int {

        if(_alert != null) {
            var result =  _alert!!.year + _alert!!.month + _alert!!.day

            _title?.forEach { c: Char ->
                result += c.toInt()
            }
            _content?.forEach { c: Char ->
                result += c.toInt()
            }
            result += _title?.length ?: 0
            result += _content?.length ?: 0
            return result
        } else
        {
            return position
        }
    }
}