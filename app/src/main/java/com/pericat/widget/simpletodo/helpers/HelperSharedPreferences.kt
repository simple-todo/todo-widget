package com.pericat.widget.simpletodo.helpers

import android.annotation.SuppressLint
import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pericat.widget.simpletodo.model.TodoTask
import java.util.*


class HelperSharedPreferences {

    companion object {

        /**
         * Permet de sauvegarder la liste des tâches à effectuer
         */
        @SuppressLint("ApplySharedPref")
        fun saveTodoList(context: Context, objet: Any, objectKey: String) {

            // Récupère les shared
            val mPrefs = context.getSharedPreferences(StaticVariables.SHARED_NAME, Context.MODE_PRIVATE)

            // Ouvre l'éditeur
            val prefsEditor = mPrefs.edit()

            // Transforme m'objet en Json avec la librairie Gson
            val gson = Gson()
            val json = gson.toJson(objet, object : TypeToken<ArrayList<TodoTask>>() {}.type) // myObject - instance of MyObject
            prefsEditor.putString(objectKey, json)
            prefsEditor.commit()
        }

        /**
         * Permet de sauvegarder la liste
         */
        @SuppressLint("ApplySharedPref")
        fun saveAlarmList(context: Context, objet: Any, objectKey: String) {

            // Récupère les shared
            val mPrefs = context.getSharedPreferences(StaticVariables.SHARED_NAME, Context.MODE_PRIVATE)

            // Ouvre l'éditeur
            val prefsEditor = mPrefs.edit()

            // Transforme m'objet en Json avec la librairie Gson
            val gson = Gson()
            val json = gson.toJson(objet, object : TypeToken<ArrayList<Date?>>() {}.type) // myObject - instance of MyObject
            prefsEditor.putString(objectKey, json)
            prefsEditor.commit()
        }

        /**
         * Permet de récupérer la liste des alarmes
         */
        fun getAlarmList(context: Context, objectKey: String): ArrayList<Date?> {

            // Récupère les shared
            val mPrefs = context.getSharedPreferences(StaticVariables.SHARED_NAME, Context.MODE_PRIVATE)

            // Récupère l'objet désiré et le désérialize avec Gson
            val gson = Gson()
            val json = mPrefs.getString(objectKey, "")

            var getObj = gson.fromJson<ArrayList<Date?>>(json, object : TypeToken<ArrayList<Date?>>() {}.type)

            if (getObj != null)
                return getObj
            else
                return ArrayList()
        }

        /**
         * Permet de récupérer la liste des tâches
         */
        fun getTodoList(context: Context, objectKey: String): ArrayList<TodoTask> {

            // Récupère les shared
            val mPrefs = context.getSharedPreferences(StaticVariables.SHARED_NAME, Context.MODE_PRIVATE)

            // Récupère l'objet désiré et le désérialize avec Gson
            val gson = Gson()
            val json = mPrefs.getString(objectKey, "")

            var getObj = gson.fromJson<ArrayList<TodoTask>>(json, object : TypeToken<ArrayList<TodoTask>>() {}.type)

            if (getObj != null)
                return getObj
            else
                return ArrayList()
        }
    }
}