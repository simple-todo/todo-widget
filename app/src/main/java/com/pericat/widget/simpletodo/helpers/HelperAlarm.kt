package com.pericat.widget.simpletodo.helpers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.google.gson.Gson
import com.pericat.widget.simpletodo.TodoWidgetProvider
import com.pericat.widget.simpletodo.model.TodoTask
import java.util.logging.Logger

class HelperAlarm {

    companion object {

        var log: Logger = Logger.getLogger(StaticVariables.LOGGER_NAME)

        /**
         * Ajoute une alarme
         */
        fun createAlarm(context: Context, task: TodoTask, position: Int?) {

            if (task._alert != null) {
                task._alert?.seconds = 0
                var realPosition = position
                if (realPosition == null) {
                    realPosition = 0
                }

                // Créer une copie de la task sérialisée
                val gson = Gson()
                val stringJson = gson.toJson(task)

                val intent = Intent(context, TodoWidgetProvider::class.java)
                intent.action = StaticVariables.ACTION_ALERT_TASK
                intent.putExtra(StaticVariables.EXTRA_ALERT_TASK, stringJson)
                val pendingIntent = PendingIntent.getBroadcast(context, task.getPositionCode(realPosition),
                        intent, PendingIntent.FLAG_UPDATE_CURRENT)

                val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.set(AlarmManager.RTC_WAKEUP, task._alert!!.time, pendingIntent)
            }
            // Si repetition
            //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, date.time, 1000*60*60*24 , pendingIntent);
        }

        /**
         * Supprime l'alarme
         */
        fun removeAlarm(context: Context, task: TodoTask, position: Int) {

            if (task._alert != null) {
                task._alert?.seconds = 0

                // Créer une copie de la task sérialisée
                val gson = Gson()
                val stringJson = gson.toJson(task)
                val intent = Intent(context, TodoWidgetProvider::class.java)
                intent.action = StaticVariables.ACTION_ALERT_TASK
                intent.putExtra(StaticVariables.EXTRA_ALERT_TASK, stringJson)
                val pendingIntent = PendingIntent.getBroadcast(context, task.getPositionCode(position),
                        intent, PendingIntent.FLAG_UPDATE_CURRENT)
                val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

                alarmManager.cancel(pendingIntent)
            }
        }
    }
}